/**
 * Copyright (c) 2021 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package myexample.bpmnotification;

import cern.japc.core.Parameter;
import cern.japc.core.ParameterException;
import cern.japc.core.Selector;

import cern.japc.core.factory.ParameterFactory;
import cern.japc.core.factory.SelectorFactory;
import cern.japc.core.factory.SimpleParameterValueFactory;
import cern.japc.core.group.GroupSubscriptionHandle;
import cern.japc.core.group.ParameterGroup;
import cern.japc.core.spi.group.ParameterGroupImpl;

public class MyExampleMain_bpmNotification {

    private static final String[] parameterNames = {
            "L4T.0227-L4T.0237-TOF/Acquisition",
            "L4P.ACAVLOOP.PIMS1112/Acquisition",
            "L4T.BPM.0227/AcquisitionPU",
            "L4T.BPM.0237/AcquisitionPU"};
 
    private static final String[] parameterNamesBak = {
            "L4D.0203-L4D.0303-TOF/Acquisition",
            "L4D.0303-L4C.0107-TOF/Acquisition",
            "L4C.0107-L4C.0207-TOF/Acquisition",
            "L4C.0107-L4C.0307-TOF/Acquisition",
            "L4C.0207-L4C.0307-TOF/Acquisition",
            "L4C.0207-L4C.0407-TOF/Acquisition",
            "L4C.0307-L4C.0407-TOF/Acquisition",
            "L4C.0307-L4C.0507-TOF/Acquisition",
            "L4C.0407-L4C.0507-TOF/Acquisition",
            "L4C.0407-L4C.0607-TOF/Acquisition",
            "L4C.0507-L4C.0607-TOF/Acquisition",
            "L4C.0507-L4C.0707-TOF/Acquisition",
            "L4C.0607-L4C.0707-TOF/Acquisition",
            "L4C.0607-L4P.0107-TOF/Acquisition",
            "L4C.0707-L4P.0107-TOF/Acquisition",
            "L4C.0707-L4P.0307-TOF/Acquisition",
            "L4P.0107-L4P.0307-TOF/Acquisition",
            "L4P.0107-L4P.0507-TOF/Acquisition",
            "L4P.0307-L4P.0507-TOF/Acquisition",
            "L4P.0307-L4P.0707-TOF/Acquisition",
            "L4P.0507-L4P.0707-TOF/Acquisition",
            "L4P.0507-L4P.0907-TOF/Acquisition",
            "L4P.0707-L4P.0907-TOF/Acquisition",
            "L4P.0707-L4P.1107-TOF/Acquisition",
            "L4P.0907-L4P.1107-TOF/Acquisition",
            "L4T.0227-L4T.0237-TOF/Acquisition",
            "L4T.1227-L4T.1245-TOF/Acquisition",
            "L4T.1227-L4T.1557-TOF/Acquisition",
            "L4T.1245-L4T.1557-TOF/Acquisition",
            "L4T.1245-L4T.1627-TOF/Acquisition",
            "L4T.1557-L4T.1627-TOF/Acquisition",
            "L4D.0203-L4D.0303-TOF/Enable",
            "L4D.0303-L4C.0107-TOF/Enable",
            "L4C.0107-L4C.0207-TOF/Enable",
            "L4C.0107-L4C.0307-TOF/Enable",
            "L4C.0207-L4C.0307-TOF/Enable",
            "L4C.0207-L4C.0407-TOF/Enable",
            "L4C.0307-L4C.0407-TOF/Enable",
            "L4C.0307-L4C.0507-TOF/Enable",
            "L4C.0407-L4C.0507-TOF/Enable",
            "L4C.0407-L4C.0607-TOF/Enable",
            "L4C.0507-L4C.0607-TOF/Enable",
            "L4C.0507-L4C.0707-TOF/Enable",
            "L4C.0607-L4C.0707-TOF/Enable",
            "L4C.0607-L4P.0107-TOF/Enable",
            "L4C.0707-L4P.0107-TOF/Enable",
            "L4C.0707-L4P.0307-TOF/Enable",
            "L4P.0107-L4P.0307-TOF/Enable",
            "L4P.0107-L4P.0507-TOF/Enable",
            "L4P.0307-L4P.0507-TOF/Enable",
            "L4P.0307-L4P.0707-TOF/Enable",
            "L4P.0507-L4P.0707-TOF/Enable",
            "L4P.0507-L4P.0907-TOF/Enable",
            "L4P.0707-L4P.0907-TOF/Enable",
            "L4P.0707-L4P.1107-TOF/Enable",
            "L4P.0907-L4P.1107-TOF/Enable",
            "L4T.0227-L4T.0237-TOF/Enable",
            "L4T.1227-L4T.1245-TOF/Enable",
            "L4T.1227-L4T.1557-TOF/Enable",
            "L4T.1245-L4T.1557-TOF/Enable",
            "L4T.1245-L4T.1627-TOF/Enable",
            "L4T.1557-L4T.1627-TOF/Enable",
            "L4D.0203-L4D.0303-TOF/Status",
            "L4D.0303-L4C.0107-TOF/Status",
            "L4C.0107-L4C.0207-TOF/Status",
            "L4C.0107-L4C.0307-TOF/Status",
            "L4C.0207-L4C.0307-TOF/Status",
            "L4C.0207-L4C.0407-TOF/Status",
            "L4C.0307-L4C.0407-TOF/Status",
            "L4C.0307-L4C.0507-TOF/Status",
            "L4C.0407-L4C.0507-TOF/Status",
            "L4C.0407-L4C.0607-TOF/Status",
            "L4C.0507-L4C.0607-TOF/Status",
            "L4C.0507-L4C.0707-TOF/Status",
            "L4C.0607-L4C.0707-TOF/Status",
            "L4C.0607-L4P.0107-TOF/Status",
            "L4C.0707-L4P.0107-TOF/Status",
            "L4C.0707-L4P.0307-TOF/Status",
            "L4P.0107-L4P.0307-TOF/Status",
            "L4P.0107-L4P.0507-TOF/Status",
            "L4P.0307-L4P.0507-TOF/Status",
            "L4P.0307-L4P.0707-TOF/Status",
            "L4P.0507-L4P.0707-TOF/Status",
            "L4P.0507-L4P.0907-TOF/Status",
            "L4P.0707-L4P.0907-TOF/Status",
            "L4P.0707-L4P.1107-TOF/Status",
            "L4P.0907-L4P.1107-TOF/Status",
            "L4T.0227-L4T.0237-TOF/Status",
            "L4T.1227-L4T.1245-TOF/Status",
            "L4T.1227-L4T.1557-TOF/Status",
            "L4T.1245-L4T.1557-TOF/Status",
            "L4T.1245-L4T.1627-TOF/Status",
            "L4T.1557-L4T.1627-TOF/Status",
            "L4D.ACAVLOOP.DTL1/Acquisition",
            "L4D.ACAVLOOP.DTL2/Acquisition",
            "L4D.ACAVLOOP.DTL3/Acquisition",
            "L4C.ACAVLOOP.CCDTL1/Acquisition",
            "L4C.ACAVLOOP.CCDTL2/Acquisition",
            "L4C.ACAVLOOP.CCDTL3/Acquisition",
            "L4C.ACAVLOOP.CCDTL4/Acquisition",
            "L4C.ACAVLOOP.CCDTL0506/Acquisition",
            "L4C.ACAVLOOP.CCDTL7/Acquisition",
            "L4P.ACAVLOOP.PIMS0102/Acquisition",
            "L4P.ACAVLOOP.PIMS0304/Acquisition",
            "L4P.ACAVLOOP.PIMS0506/Acquisition",
            "L4P.ACAVLOOP.PIMS0708/Acquisition",
            "L4P.ACAVLOOP.PIMS0910/Acquisition",
            "L4P.ACAVLOOP.PIMS1112/Acquisition",
            "L4T.ACAVLOOP.DEBUNCHER/Acquisition"};
 
    
    
    
    
    public static void main(final String... args)  
    {
        
        System.out.println("Hello world");
     
        ParameterGroup p = null;
        try {
            p = createParameterGroup(parameterNames);
            
            //L4T.BPM.0227
        } catch (ParameterException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Selector s = SelectorFactory.newSelector("PSB.USER.NORMHRS");
        
        //_________________________________
        try
        {
          String parameterName = "L4D.0203-L4D.0303-TOF/Setting#energyIndex";
          Parameter parameter = ParameterFactory.newInstance().newParameter(parameterName);
          parameter.setValue(s,SimpleParameterValueFactory.newSimpleParameterValue(15));
        }
        catch(Exception e)
        {
            System.err.println("Could not set the cavity index");
            return;
        }
        
        //_________________________________
        
        MyParameterValueListener pvl = new MyParameterValueListener();
         
        //create a subscription handle
        GroupSubscriptionHandle sh = p.createSubscription(s, pvl);
       
        //starting the subscription
        try {
            sh.startMonitoring();
        } catch (ParameterException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
         
        try {
            Thread.sleep(300000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
         
        //stop the subscription
        sh.stopMonitoring();
    }
    private static ParameterGroup createParameterGroup(String[] parameterNames) throws ParameterException {
        ParameterFactory factory = ParameterFactory.newInstance();
        Parameter[] parameters = new Parameter[parameterNames.length];
        for (int ix = 0; ix < parameterNames.length; ix++) {
          parameters[ix] = factory.newParameter(parameterNames[ix]);
        }
        ParameterGroup grp = new ParameterGroupImpl();
        grp.addAll(parameters);
        return grp;
      }

    
}
