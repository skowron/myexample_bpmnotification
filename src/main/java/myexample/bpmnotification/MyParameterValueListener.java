/**
 * Copyright (c) 2021 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package myexample.bpmnotification;

import java.util.Iterator;
import java.util.LinkedList;

import cern.japc.core.FailSafeParameterValue;
import cern.japc.core.group.FailSafeParameterValueListener;
import cern.japc.value.MapParameterValue;
import cern.japc.value.ParameterValue;

public class MyParameterValueListener implements FailSafeParameterValueListener {

    LinkedList<Long> pRF = new LinkedList<>();

    @Override
    public void valueReceived(FailSafeParameterValue[] values) 
    {
        
        //System.out.println("Value received ");
        if (values == null)
        {
            System.err.println("Got null array");
            return;
        }
        //System.out.println("Value received len = "+ values.length);
        
        Long stampRF=99L, stampBPM=99L, stampTOF=99L;
        
        for (FailSafeParameterValue pv: values)
        {   
            if(pv.getHeader().isFirstUpdate())
                continue;
            
            if( pv.getParameterName().contains("ACAVLOOP") )
              {
                stampRF = pv.getHeader().getCycleStampMillis();
              }
            
            if( pv.getParameterName().contains("BPM") )
            {
               long st = pv.getHeader().getCycleStampMillis();
               
               if (stampBPM == 99)
               {
                   stampBPM = st;
               }
               else if (st != stampBPM)
               {
                   System.err.println("BPMs have different cycle stamp! "+st+" "+stampBPM);
               }
               
               long sta = pv.getHeader().getAcqStampMillis();
               
               if (Math.abs(sta - st) > 1000)
               {
                   System.out.println("BPM AQN is late " + (st - sta));
               }
               
            }

            if( pv.getParameterName().contains("-TOF") )
            {
               long st = pv.getHeader().getCycleStampMillis();
               ParameterValue v = pv.getValue();
               if (v == null)
               {
                   System.err.println(pv.getParameterName() + " has no value!");
                   continue;
               }
               MapParameterValue vm = v.castAsMap();
               if (vm == null)
               {
                   System.err.println(pv.getParameterName() + " not a map value!");
                   continue;
               }
               stampTOF = vm.getLong("recCycleStamp")/1000000;
            }
            //System.out.println( v.toString());
        }
            
      // System.out.println(" "+stampRF+" "+stampBPM+" "+stampTOF);
       if (pRF.size() > 1)
       {
           long delayOfTOF = (stampTOF - stampRF )/1200;
           System.out.print(" RF stamp "+stampRF);
           System.out.print(" | Delay of BPM "+ (stampBPM - stampRF)/1200 + " |  Delay TOF:"+delayOfTOF);
           
           if (delayOfTOF == 0)
           {
               System.out.println(" | TOF hand no delay. Success!!!");
           }
           else
           {
               System.out.print(" | >>> Delay of TOF should be ");
               boolean found = false;
               Iterator<Long> liter = pRF.descendingIterator();
               while( liter.hasNext() )
               {   long st = liter.next();
               long delta = (st - stampRF)/1200;
               if (delta < -1e9) delta = 0;
               System.out.print(delta);
               if (delayOfTOF == delta)
               {
                   found = true;
                   break;
               }
               System.out.print(" or ");

               }
               if (found)
               {
                   System.out.println(" OK");

               }
               else
               {
                   System.out.println(" NOT OK");
               }
           }
           
       }
       
       pRF.add(stampRF);
       if (pRF.size() > 5)
       {
           pRF.removeFirst();
       }
        
    }



}
